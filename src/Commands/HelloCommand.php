<?php

namespace Pretzel\Commands;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class HelloCommand extends Command
{
	protected $name = 'start';

	public function handle()
	{
		$argumentsMessage = json_encode($arguments);
		//Start session
		session_set_cookie_params(3600, "/");
		session_start();
		$userRegistrasiBot = $_SESSION['user'];

		//read incoming info and grab the chatID
		$content = file_get_contents("php://input");
		$update = json_decode($content, true);
		$chatID = $update["message"]["chat"]["id"];
		$message = $update["message"]["text"];
		$reply = $update;

		// logger
		$log = new Logger('name');
		$log->pushHandler(new StreamHandler('log/test.log', Logger::DEBUG));
		$log->pushHandler(new FirePHPHandler());

		$this->replyWithMessage([
			'text' => "Hello what's your name ?"
		]);

		$this->replyWithChatAction(['action' => Actions::TYPING]);


		$sendto = API_URL."sendmessage?chat_id=".$chatID."&text=".$update;
		file_get_contents($sendto);

		// add records to the log
		$log->info('Information message ', ['message' => $argumentsMessage]);

	}
}
